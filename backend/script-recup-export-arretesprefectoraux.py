import pandas as pd
import requests
from bs4 import BeautifulSoup

# Récupération de la liste des départements concernés par un arrêté préfectoral

lien_page_arretes = "https://ambroisie-risque.info/reglementation/#les-arretes-prefectoraux-par-departement"

def recup_export():
    page_liste_arretes = requests.get(lien_page_arretes)
    soup = BeautifulSoup(page_liste_arretes.content, "html.parser")
    partie_arretes = soup.find(id="les-arretes-prefectoraux-par-departement").find("table")
    tableau_arretes = pd.read_html(str(partie_arretes))[0]
    liste_dpts_avec_arrete = tableau_arretes.iloc[1:,0].copy().rename("departements-noms").reset_index(drop=True)
    liste_dpts_avec_arrete.to_csv("liste_dpts_avec_arrete.csv", sep=";", index=False)

recup_export()