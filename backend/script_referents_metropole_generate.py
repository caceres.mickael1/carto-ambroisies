import os
import geopandas as gpd
import pandas as pd


def generate_referents_metropole():
    communes_fr = pd.read_csv("georef-france-commune.csv",
                 sep=";",
                 header=0,
                 usecols=[0,5,17],
                 names=["geom_point", "code_dpt", "code_insee"],
                 dtype={"code_insee": 'str', "code_dpt": 'str'}
                )

    communes_fr = (communes_fr
                   .sort_values(by="code_insee", ascending=True)
                   .reset_index(drop=True).copy()
                   .query('code_dpt.str.len() < 3', engine='python').copy()
                   .assign(referent=False)
                   .drop(columns = ["code_dpt"])
                   )
    
    communes_fr[["lon", "lat"]] = communes_fr["geom_point"].str.split(',', expand=True)
    
    referents_fictifs = pd.read_csv("referents_fictifs.csv", 
                                dtype={"code_insee": 'str'})
    
    cod_com = (referents_fictifs["code_insee"]).tolist()
    
    idx = communes_fr.query("code_insee in @cod_com")
    
    communes_fr.iloc[[idx.index], [3]] = True
    
    
    gdf = gpd.GeoDataFrame(communes_fr, crs = "EPSG:4326", geometry=gpd.points_from_xy(communes_fr.lon, communes_fr.lat))
    
    gdf = gdf.to_crs("EPSG:2154")
    
    gdf.to_file("referents_metropole_generated.geojson", driver='GeoJSON')

generate_referents_metropole()
