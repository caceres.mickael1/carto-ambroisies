###
### Chargement des Libraries
###
import os
import fnmatch
import geopandas as gpd
import pandas as pd
import numpy as np
from pyproj import Transformer




def Cleaning_ambroisie(): 
    ###
    ### I - Importation des données
    ###

    list_of_dfs = []
    for path,dirs,files in os.walk("Signalements ambroisie"):
        for file in files:
            if fnmatch.fnmatch(file,'*.dbf'):
                fullname = os.path.join(path,file)
                #print(fullname)
                df = gpd.read_file(fullname)
                df["espece"]=fullname.split("\\")[2]  
                #print(df.info())
                #print(" ")
                #print('#########'*10)
                #print(" ")
                list_of_dfs.append(df)
                
    ###
    ### II - Selection des colonnes d'intérêts
    ###

    list_of_dfs[0]=list_of_dfs[0][["espece","date_sb_da","densite","statut","nom_com","insee_com","insee_dep","x","y"]]
    list_of_dfs[1]=list_of_dfs[1][["espece","date_sb_da","densite","statut","commune","code_insee","code_dpt","x","y"]]
    list_of_dfs[2]=list_of_dfs[2][["espece","date_sb_da","densite","statut","commune","code_insee","code_dpt","x","y"]]
    list_of_dfs[3]=list_of_dfs[3][["espece","date_sb_da","densite","statut","commune","code_insee","code_dpt","x","y"]]
    list_of_dfs[4]=list_of_dfs[4][["espece","c_date_sb_","c_densite","c_statut","c_commune","c_code_ins","c_code_dpt","c_x","c_y"]]
    list_of_dfs[5]=list_of_dfs[5][["espece","ars_metr_4","ars_metr_5","ars_metr_6","nom","insee","ars_metr_8",'ars_metr_1','ars_metr_2']]

    list_of_dfs[6]=list_of_dfs[6][["espece","ars_metr_1","ars_metr_4","ars_metr_7","nom","insee","ars_metr_6"]]
    list_of_dfs[6][["x","y"]]=np.nan #Ambroisie à feuille d'armoise 2016 ne présente pas de données X et Y. Par la suite, la position de la ville sera utilisée.

    list_of_dfs[7]=list_of_dfs[7][["espece","ars_metr_1","ars_metr_4","ars_metr_7","nom","insee","ars_metr_6",'ars_metr14','ars_metr15']]
    list_of_dfs[8]=list_of_dfs[8][["espece","ars_metr_1","ars_metr_4","ars_metr_6","nom","insee","ars_metr17",'ars_metr12','ars_metr13']]
    list_of_dfs[9]=list_of_dfs[9][["espece","ars_metr_1","ars_metr_4","ars_metr_7","nom","insee","ars_metr_6",'ars_metr17','ars_metr18']]
    list_of_dfs[10]=list_of_dfs[10][["espece","ars_metr_1","ars_metr_4","ars_metr_7","nom","insee","ars_metr_6",'ars_metr17','ars_metr18']]
    list_of_dfs[11]=list_of_dfs[11][["espece","ars_metr_1","ars_metr_4","ars_metr_6","nom","insee","ars_metr_5",'ars_metr13','ars_metr14']]
    list_of_dfs[12]=list_of_dfs[12][["espece","ars_metr_2","ars_metr_4","ars_metr_6","nom","insee","ars_metr_5",'ars_metr11','ars_metr12']]
    list_of_dfs[13]=list_of_dfs[13][["espece","date_sb_da","densite","statut","commune","code_insee","code_dpt",'x','y']]
    list_of_dfs[14]=list_of_dfs[14][["espece","date_sb_da","densite","statut","commune","code_insee","code_dpt",'x','y']]
    list_of_dfs[15]=list_of_dfs[15][["espece","c_date_sb_","c_densite","c_statut","c_commune","c_code_ins","c_code_dpt","c_x","c_y"]]

    ###
    ### III - Renommage des colonnes, en accord avec le schéma actuelle des données  (WFS), et consolidation du jeu de données
    ###

    cols = ["properties.c_espece","properties.c_date_sb_data_1","properties.c_densite","properties.c_statut","properties.c_commune","properties.c_code_insee","properties.c_code_dpt","properties.c_x","properties.c_y"]
    for i in range(len(list_of_dfs)):
        list_of_dfs[i]=list_of_dfs[i].set_axis(cols, axis=1)
        
    df = pd.concat(list_of_dfs)

    ###
    ### IV - Nettoyage des données
    ###

    # Homogénéinité des départements
    df["properties.c_code_dpt"].replace('1', '01',inplace=True)
    df["properties.c_code_dpt"].replace('69250', '69',inplace=True)

    # Homogénéinité de la densité
    df["properties.c_densite"].replace(['InfÃ©rieur Ã\xa0 10', 'Inferieur a 10'], 'Inférieur à 10',inplace=True)
    df["properties.c_densite"].replace(['SupÃ©rieur a 50','Superieur a 50', 'Supérieur a 50'], 'Supérieur à 50',inplace=True)
    #Il reste 26 lignes (ambroisie feuille d'armoise 2016 sans info)

    # Homogénéinité du statut
    df['properties.c_statut'].replace(['validÃ© non dÃ©truit','validé non détruit'],
                                    'Validé non détruit',inplace=True)
    df['properties.c_statut'].replace(['validÃ© dÃ©truit','validé détruit'],
                                    'Validé détruit',inplace=True)
    df['properties.c_statut'].replace(['erronée confirmation par FREDON Aura',
                                    'erroné confirmation guillaume Fried'],
                                    "Signalement erroné", inplace=True)
    df['properties.c_statut'].replace(['à valider prospection prévues en 2023',
                                    'à valider prospection prévues en 2023'],
                                    'À valider', inplace=True)

    # Suppression d'une ligne ne contenant aucune information (uniquement ville)
    df=df.dropna(subset=["properties.c_code_dpt","properties.c_densite","properties.c_statut"], how='all').reset_index(drop=True)

    ## Gestion des données manquantes et aberrantes : 
    # Ambroisie à feuille d'armoise 2015 : Dates manquantes (remplacées par "2015-01-01") + Coordonnées X et Y de dans un référentiel inconnu

    df["properties.c_date_sb_data_1"]=df["properties.c_date_sb_data_1"].fillna("2015-01-01")
    df["properties.c_date_sb_data_1"]=pd.to_datetime(df["properties.c_date_sb_data_1"])


    df["year"]=df["properties.c_date_sb_data_1"].dt.strftime('%Y')

    # Suppression des coordonnées X et Y :
    df['properties.c_x'] = np.where((df['year'] == "2015") & (df['properties.c_espece'] == 'Ambroisie à feuilles d_armoise'), np.nan, df['properties.c_x'])
    df['properties.c_y'] = np.where((df['year'] == "2015") & (df['properties.c_espece'] == 'Ambroisie à feuilles d_armoise'), np.nan, df['properties.c_y'])

    df["properties.c_date_sb_data_1"]=df["properties.c_date_sb_data_1"].astype(str)


    ## Remplissage des données manquantes :
    # Chargement des infos géographiques des communes françaises. Source : Opendatasoft, last update : 27 septembre 2022
    communes_fr=pd.read_csv("georef-france-commune.csv",sep=";", usecols=['Geo Point','Code Officiel Commune','Nom Officiel Commune'])

    # Jointure entre le dataframe et les communes
    df=df.merge(communes_fr, left_on="properties.c_code_insee", right_on='Code Officiel Commune')

    # séparation des coordonnées géographiques des communes
    df[["lon","lat"]]=df['Geo Point'].str.split(',', expand=True)

    # Transformation des coordonnées des communes en Lambert-93
    transformer = Transformer.from_crs("4326", "2154")
    x, y = transformer.transform(df['lon'], df['lat'])

    # Remplissage des coord. manquantes de 2016 et 2015 (précédemment supprimées) par les coord. des villes
    pd.options.display.float_format = '{:.8f}'.format
    df["properties.c_x"] = df["properties.c_x"].fillna(pd.Series(x))
    df["properties.c_y"] = df["properties.c_y"].fillna(pd.Series(y))


    # Filtre des colonnes d'intérêts : Le nom des communes et leur code est plus homogène dans les données "communes_fr"
    df = df[["properties.c_espece","properties.c_date_sb_data_1","properties.c_densite","properties.c_statut","Nom Officiel Commune","Code Officiel Commune","properties.c_code_dpt","properties.c_x","properties.c_y"]]

    # Renommage des colonnes
    df.rename(
        columns={"Nom Officiel Commune": "properties.c_commune", "Code Officiel Commune": "properties.c_code_insee"},
        inplace=True,
    )


    ###
    ### V - Création d'un GeoDataFrame 
    ###
    
    gpd.options.display_precision = 8
    gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df["properties.c_x"], df["properties.c_y"]),crs=2154)

    #Export des données en GeoJson
    gdf.to_file("signalements_ambroisies_clean.geojson", driver='GeoJSON')

Cleaning_ambroisie()